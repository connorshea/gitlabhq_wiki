## From 1.0.x to 1.1.x


### 1. Update code base from `1x` or `1-stable` or `master` branch

```bash

# from github
git pull origin 1x

# update libs
bundle install

# update db
bundle exec rake db:migrate RAILS_ENV=production


# give your server user access to remove git repo 
# Ex.
#   If you are going to use user 'gitlabhq' for rails server
#   gitlabhq ALL = (git) NOPASSWD: /bin/rm" | sudo tee -a /etc/sudoers
#
echo "USERNAME ALL = (git) NOPASSWD: /bin/rm" | sudo tee -a /etc/sudoers

```

### 2. Restart server


## From 1.1.0 to 1.1.1

### 1. Update code base from `1x` or `1-stable` or `master` branch

```bash

# from github
git pull origin 1x

# update libs
bundle install

# update db
bundle exec rake db:migrate RAILS_ENV=production


```

### 2. Restart server
### Preparing

* install git
* generate ssh key

### 1. [[Ruby]]  (skip if exists)

### 2. [[Gitolite]]

### 3. Install packages required for gitlabhq

```bash
sudo apt-get install python-dev python-pip sendmail redis-server  libicu-dev
sudo pip install pygments
sudo gem install bundler
```

### 4. Install gitlabhq

```bash
git clone git://github.com/gitlabhq/gitlabhq.git
cd gitlabhq
bundle install --without development test
sudo gem install charlock_holmes
bundle exec rake db:setup RAILS_ENV=production
bundle exec rake db:seed_fu RAILS_ENV=production

# Rename config files
cp config/database.yml.example config/database.yml
cp config/gitlab.yml.example config/gitlab.yml

```

### 5. Start server

`bundle exec rails s -e production`

### 6. Visit [http://localhost:3000](http://localhost:3000)

### 7. Login with credentials:

User - `admin@local.host`

Password - `5iveL!fe`

This guide allow to quickly setup gitolite v3 from scratch saving existing repositories

### 1. Move all your repositories in safe place

```bash
sudo mv /home/git/repositories /home/gitlab/bk/
```

### 2. Remove git user with home dir

```bash
sudo userdel -r git
```

### 3. Add git user

```bash

    sudo adduser \
      --system \
      --shell /bin/sh \
      --gecos 'git version control' \
      --group \
      --disabled-password \
      --home /home/git \
      git

```

### 4. Add git to gitlab user and gitlab to git

```bash
sudo usermod -a -G git gitlab
```

### 5. Clone latest gitolite and setup it

```bash
sudo -H -u git git clone -b gl-v320 https://github.com/gitlabhq/gitolite.git /home/git/gitolite
cd /home/git
sudo -u git -H mkdir bin
sudo -u git sh -c 'echo -e "PATH=\$PATH:/home/git/bin\nexport PATH" >> /home/git/.profile'
sudo -u git sh -c 'gitolite/install -ln /home/git/bin'
sudo cp /home/gitlab/.ssh/id_rsa.pub /home/git/gitlab.pub
sudo chmod 0444 /home/git/gitlab.pub
sudo -u git -H sh -c "PATH=/home/git/bin:$PATH; gitolite setup -pk /home/git/gitlab.pub"
```

### 6. Copy repos back

```bash
cd /home/gitlab/bk
sudo cp -R ./  /home/git/repositories
sudo chmod -R ug+rwXs,o-rwx /home/git/repositories/
sudo chown -R git:git /home/git/repositories/
```

### 7. Test gitolite clone

```bash
sudo -u gitlab -H git clone git@localhost:gitolite-admin.git /tmp/gitolite-admin
```

### 8. Copy hooks

```bash
cd /home/gitlab/gitlab/
sudo cp ./lib/hooks/post-receive /home/git/.gitolite/hooks/common/post-receive
sudo chown git:git /home/git/.gitolite/hooks/common/post-receive
```

### 9. Move keys, repos to gitolite config from gitlab app

```bash
sudo -u gitlab -H bundle exec rake gitlab:gitolite:update_keys RAILS_ENV=production
sudo -u gitlab -H bundle exec rake gitlab:gitolite:update_repos RAILS_ENV=production

# for GitLab 3.1 and earlier
sudo -u gitlab -H bundle exec rake gitlab:app:enable_automerge RAILS_ENV=production
# for GitLab 4.0 and later
sudo -u gitlab -H bundle exec rake gitlab:enable_automerge RAILS_ENV=production
```
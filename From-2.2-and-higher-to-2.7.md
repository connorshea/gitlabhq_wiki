### 1. Stop server & resque

### 2. Follow instructions

```bash

# Get latest code
git pull origin stable

# Update rubygems
gem update --system

# We changed config format so replace with new default and edit
cp config/gitlab.yml.example config/gitlab.yml 

# Install gems
bundle install --without development test

# Migrate db
bundle exec rake db:migrate RAILS_ENV=production

# Update hooks
bundle exec rake gitlab:app:update_hooks RAILS_ENV=production

# Enable automerge
bundle exec rake gitlab:app:enable_automerge RAILS_ENV=production

# Check APP Status
bundle exec rake gitlab:app:status RAILS_ENV=production
```

### 3. Start all
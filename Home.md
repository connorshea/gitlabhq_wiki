This wiki is not used, please see the [README](https://github.com/gitlabhq/gitlabhq/blob/master/README.md) for general information.

Please consult the [new versions and upgrading](https://github.com/gitlabhq/gitlabhq#new-versions-and-upgrading) section in the README for upgrading information.

Please see the [public wiki](https://github.com/gitlabhq/gitlab-public-wiki/wiki) for unofficial installation and extension guides.

### Deprecated components upgrade guides

* [[Update Gitolite]] (only relevant for 4.2 and older)
* [[Reinstall Gitolite]] (only relevant for 4.2 and older)
* [[Migrate from SQLite to MySQL]] (only relevant for 3.1 and older)
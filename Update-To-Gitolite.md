### Update gitlab to gitolite 

0. get latest code from master
1. Backup /home/git/repositories
2. Setup gitolite with repository $REPO_UMASK = 0007 by editing the .gitolite.rc file for the Gitolite user
3. Restore repositories dir
  * Run `chmod -R 770  repositories/` for imported repositories
  * Run `chown -R git:git repositories/` for imported repositories
4. Edit `gitlab.yml` with right config
5. run `bundle exec rake gitolite_rebuild RAILS_ENV=production`. this will recreate all users & projects at gitolite config
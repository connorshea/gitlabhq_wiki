GitLab can create projects for your repos automatically

1. Copy **bare** repositories to `/home/git/repositories`
2. Run `bundle exec rake gitlab:import:repos RAILS_ENV=production`
3. PROFIT

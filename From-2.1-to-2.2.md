### 1. Update code & db

```bash
# Get latest code
git pull origin stable

# Rename config files
cp config/database.yml.example config/database.yml
cp config/gitlab.yml.example config/gitlab.yml

# Install libs
bundle install --without development test

# update db
bundle exec rake db:migrate RAILS_ENV=production
```

### 2. Restart server
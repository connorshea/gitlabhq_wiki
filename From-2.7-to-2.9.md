### 1. Stop server & resque


### 2. Update code & db


```bash
# Get latest code
sudo -u gitlab git pull origin stable

# Install libs
sudo -u gitlab bundle install --without development test

# update db
sudo -u gitlab bundle exec rake db:migrate RAILS_ENV=production

# update gitolite hooks
sudo cp ./lib/hooks/post-receive /home/git/share/gitolite/hooks/common/post-receive
sudo chown git:git /home/git/share/gitolite/hooks/common/post-receive

# Check app status
sudo -u gitlab bundle exec rake gitlab:app:status RAILS_ENV=production


```


Compare your config/gitlab.yml & config/gitlab.yml.example for new settings

__Especially for LDAP__


### 3. Start web server & Resque
### 1. Update code & db

```bash

# from github
git pull origin master

# install some packages
sudo apt-get install redis-server libicu-dev

# install gems
sudo gem install charlock_holmes -v '0.6.8'
bundle install --without development test

# update db
bundle exec rake db:migrate RAILS_ENV=production

```

### 2. Restart server

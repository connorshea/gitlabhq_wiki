### 1. Stop server & resque

    sudo service gitlab stop

### 2. Follow instructions

```bash

# Get latest code
git pull origin stable

# Install gems
bundle install --without development test

# Migrate db
bundle exec rake db:migrate RAILS_ENV=production

# Check APP Status
bundle exec rake gitlab:app:status RAILS_ENV=production

```

Compare your config/gitlab.yml & config/gitlab.yml.example for new settings

__Especially for LDAP__

### 3. Start all

    sudo service gitlab start
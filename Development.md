## Moderno - Version 2.0 

### We already finished:

* Merge requests
* gitolite support
* Team permissions improved
* easy installation scripts for ubuntu
* all-in-one config file `config/gitlab.yml`
* improved commit page


### We currently working on:

* fixing design issues
* refactoring js codebase
* allow to select default branch other than `master`
* manage repository branches
* backend code refactoring

### We are looking for help with:

* better documentation
* install scripts for other os
* script for update `.gitolite.rc` file with umask 0007
* wiki feature
* any ideas to help project
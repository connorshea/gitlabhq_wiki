### 1. Stop server & resque


### 2. Update code & db


```bash
# You may want to add 
# sudo -H -u gitlab 
# before each script

# Get latest code
git pull origin stable

# !!! Config should be replaced with a new one. Check it after replace
cp config/gitlab.yml.example config/gitlab.yml

# Install libs
bundle install --without development test

# update db
bundle exec rake db:migrate RAILS_ENV=production

```

### 3. Start web server & Resque
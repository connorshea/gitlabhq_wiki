GitLab ceases to support SQLite from v4.0 onwards. If you are still using SQLite follow these steps in order to migrate your data from SQLite to MySQL.

1. Update to the latest GitLab
2. Run `bundle exec rake db:data:dump RAILS_ENV=production` to extract the data currently stored in SQLite.
3. Follow the [database setup guide](https://github.com/gitlabhq/gitlabhq/blob/master/doc/install/databases.md) to install MySQL and update your `database.yml`
4. Run `bundle exec rake db:setup RAILS_ENV=production` to setup your database tables in MySQL.
5. Run `bundle exec rake db:data:load RAILS_ENV=production` to load the data saved in step 2 into MySQL.
6. Rename your `db/production.sqlite` file (don't delete it yet).
7. Restart GitLab and test if all the data has been transfered correctly.
8. Delete your `db/*.sqlite` files, if you are sure everything worked.



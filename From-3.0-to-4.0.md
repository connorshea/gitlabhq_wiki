### 1. Setup charlock_holmes

```

# Install new charlock_holmes
sudo gem install charlock_holmes --version '0.6.9'


```

### 2. Follow [update guide for 3.1 - to 4.0](https://github.com/gitlabhq/gitlabhq/wiki/From-3.1-to-4.0)
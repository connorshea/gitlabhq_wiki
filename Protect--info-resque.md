### If version is lower 2.6.2

1. First copy file with auth
`cp config/initializers/protect_resque.rb.example config/initializers/protect_resque.rb`
2. Edit `config/initializers/protect_resque.rb` with your login/password
3. Restart Application

### In 2.6.2 it already included.

You can change User/Pass in `config/initializers/protect_resque.rb`
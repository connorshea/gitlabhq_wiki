# Gitlab Modules

There is talk about making Gitlab more modular in order to better select functionality needed by different people.

Before we go out and re-write Gitlab to be entirely modular I'd like to gather information about what modules are planned/proposed, and more importantly, what they'd need from Gitlab to function properly.

Please copy the following template and let us know what you'd need from Gitlab to write a successful module.

## Sample Module Proposal

### Description

My module will do this and that and allow users to be 200% more productive.

### Requirements

 * What does your module need from Gitlab?
 * Does it require hooks? If so, where?
 * Does it require an API? If so, what?
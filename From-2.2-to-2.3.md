### 1. Update code & db

```bash
# Get latest code
git pull origin stable

# Install libs
bundle install --without development test

# update db
bundle exec rake db:migrate RAILS_ENV=production

# Update hooks
bundle exec rake update_hooks RAILS_ENV=production

```

### 2. Restart web server


### 3. Restart (or start) Resque

```bash
./resque.sh

```

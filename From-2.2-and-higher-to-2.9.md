### 1. Stop server & resque

### 2. Follow instructions

```bash

# Get latest code
sudo -u gitlab git pull origin stable

# Update rubygems
sudo -u gitlab gem update --system

# We changed config format so replace with new default and edit
sudo -u gitlab cp config/gitlab.yml.example config/gitlab.yml 

# Install gems
sudo -u gitlab bundle install --without development test

# Migrate db
sudo -u gitlab bundle exec rake db:migrate RAILS_ENV=production

# update gitolite hooks
sudo cp ./lib/hooks/post-receive /home/git/share/gitolite/hooks/common/post-receive
sudo chown git:git /home/git/share/gitolite/hooks/common/post-receive

# Enable automerge
sudo -u gitlab bundle exec rake gitlab:app:enable_automerge RAILS_ENV=production

# Check APP Status
sudo -u gitlab bundle exec rake gitlab:app:status RAILS_ENV=production
```

### 3. Start all
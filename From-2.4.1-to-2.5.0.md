### 1. Stop server & resque


### 2. Update code & db


```bash
# You may want to add 
# sudo -H -u gitlab 
# before each script

# Get latest code
git pull origin stable

# Install libs
bundle install --without development test

# If you have problem with bundle install like
# Could not find carrierwave-0.6.2 in any of the sources
gem update --system

# update db
bundle exec rake db:migrate RAILS_ENV=production

```

### 3. Start web server & Resque
1. Run this

```

git pull origin stable

# install some packages
sudo apt-get install redis-server libicu-dev

# Rename config files
cp config/database.yml.example config/database.yml
cp config/gitlab.yml.example config/gitlab.yml

# install gems
sudo gem install charlock_holmes -v '0.6.8'
bundle install --without development test

# update db
bundle exec rake db:migrate RAILS_ENV=production


```

### 2. Restart server
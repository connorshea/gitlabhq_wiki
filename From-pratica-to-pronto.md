### 1. Update code & db

```bash

# from github
git pull origin master

# update libs
bundle install

# update db
bundle exec rake db:migrate RAILS_ENV=production


# give your server user access to remove git repo 
# Ex.
#   If you are going to use user 'gitlabhq' for rails server
#   gitlabhq ALL = (git) NOPASSWD: /bin/rm" | sudo tee -a /etc/sudoers
#
echo "USERNAME ALL = (git) NOPASSWD: /bin/rm" | sudo tee -a /etc/sudoers

```

### 2. Restart server

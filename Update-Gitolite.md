In GitLab 2.9.x there was a bug which can lead to bad consequences such as [#1754](https://github.com/gitlabhq/gitlabhq/issues/1754) or [#1709](https://github.com/gitlabhq/gitlabhq/issues/1709). That solves them. Also that steps related to users who wants to update gitolite v2 to v3.

#### 1. Backup all repositories. Just copy /home/git/repositories/* elsewhere.

#### 2. Install Gitolite from scratch. See the [Gitolite section in the Installation Guide](https://github.com/gitlabhq/gitlabhq/blob/master/doc/install/installation.md#4-gitolite).

#### 3. Copy repositories back.

#### 4. Update gitolite config

    sudo -u gitlab -H bundle exec rake gitlab:gitolite:update_keys RAILS_ENV=production
    sudo -u gitlab -H bundle exec rake gitlab:gitolite:update_repos RAILS_ENV=production

#### 5. Copy hooks

    sudo cp ./lib/hooks/post-receive /home/git/.gitolite/hooks/common/post-receive
    sudo chown git:git /home/git/.gitolite/hooks/common/post-receive


#### 6. Check if everything is OK

    sudo -u gitlab -H bundle exec rake gitlab:check RAILS_ENV=production

That's all.
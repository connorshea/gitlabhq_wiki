### 1. Update code & db

```bash
# Get latest code
git pull origin stable

# Install libs
bundle install --without development test

# Enable automerge
bundle exec rake gitlab:app:enable_automerge RAILS_ENV=production

```

### 2. Restart web server & Resque
